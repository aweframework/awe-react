
# Changelog for AWE React 1.0.5
*26/11/2024*

- When adding or removing a class on add-class or remove-class actions, node may be not defined and it crashes. [MR #74](https://gitlab.com/aweframework/awe-react/-/merge_requests/74) (Pablo Javier García Mora)

# Changelog for AWE React 1.0.4
*21/11/2024*

- Pick the new AWE version. [MR #73](https://gitlab.com/aweframework/awe-react/-/merge_requests/73) (Pablo Javier García Mora)
- Fix chart animation issues on show. [MR #72](https://gitlab.com/aweframework/awe-react/-/merge_requests/72) (Pablo Javier García Mora)

# Changelog for AWE React 1.0.3
*20/11/2024*

- Fix some chart issues (formatting options for labels). [MR #71](https://gitlab.com/aweframework/awe-react/-/merge_requests/71) (Pablo Javier García Mora)
- Allow avatar to be used without values in model (only with attributes). [MR #70](https://gitlab.com/aweframework/awe-react/-/merge_requests/70) (Pablo Javier García Mora)
- When losing session the engine shows error 500 instead of returning to signin screen. [MR #69](https://gitlab.com/aweframework/awe-react/-/merge_requests/69) (Pablo Javier García Mora)

# Changelog for AWE React 1.0.2
*11/11/2024*

- Allow to add new languages in application (highcharts bug). [MR #68](https://gitlab.com/aweframework/awe-react/-/merge_requests/68) (Pablo Javier García Mora)

# Changelog for AWE React 1.0.1
*06/11/2024*

- Allow HTML from markdown on toast messages. [MR #67](https://gitlab.com/aweframework/awe-react/-/merge_requests/67) (Pablo Javier García Mora)
- Use `size` attribute in criteria and buttons. [MR #66](https://gitlab.com/aweframework/awe-react/-/merge_requests/66) (Pablo Javier García Mora)
- * Fixed issue. [MR #65](https://gitlab.com/aweframework/awe-react/-/merge_requests/65) (Pablo Javier García Mora)

# Changelog for AWE React 1.0.0
*31/10/2024*

- Add PDF Viewer widget. [MR #64](https://gitlab.com/aweframework/awe-react/-/merge_requests/64) (Pablo Javier García Mora)
- Add a new component for videos. [MR #63](https://gitlab.com/aweframework/awe-react/-/merge_requests/63) (Pablo Javier García Mora)
- Add a new component for links. [MR #62](https://gitlab.com/aweframework/awe-react/-/merge_requests/62) (Pablo Javier García Mora)
- Add a new component for images. [MR #61](https://gitlab.com/aweframework/awe-react/-/merge_requests/61) (Pablo Javier García Mora)
- Pager-values is not being used in pagination. [MR #60](https://gitlab.com/aweframework/awe-react/-/merge_requests/60) (Pablo Javier García Mora)
- Improve confirm message style. [MR #41](https://gitlab.com/aweframework/awe-react/-/merge_requests/41) (Pablo Javier García Mora)
- Add vertical menu depending on style. [MR #37](https://gitlab.com/aweframework/awe-react/-/merge_requests/37) (Pablo Javier García Mora)
- Add material design icons. [MR #36](https://gitlab.com/aweframework/awe-react/-/merge_requests/36) (Pablo Javier García Mora)
- Finish component actions. [MR #35](https://gitlab.com/aweframework/awe-react/-/merge_requests/35) (Pablo Javier García Mora)
- Add Chart actions. [MR #34](https://gitlab.com/aweframework/awe-react/-/merge_requests/34) (Pablo Javier García Mora)
- Add Grid context menu. [MR #30](https://gitlab.com/aweframework/awe-react/-/merge_requests/30) (Pablo Javier García Mora)
- Add missing Form Actions. [MR #29](https://gitlab.com/aweframework/awe-react/-/merge_requests/29) (Pablo Javier García Mora)
- Hide and show columns dependency is not working. [MR #27](https://gitlab.com/aweframework/awe-react/-/merge_requests/27) (Pablo Javier García Mora)
- Format components before sending as print data. [MR #26](https://gitlab.com/aweframework/awe-react/-/merge_requests/26) (Pablo Javier García Mora)
- Bump AWE to 4.7.9 version. [MR #25](https://gitlab.com/aweframework/awe-react/-/merge_requests/25) (Pablo Javier García Mora)
- Add Treegrid. [MR #24](https://gitlab.com/aweframework/awe-react/-/merge_requests/24) (Pablo Javier García Mora)
- Update project to awe v4.6.0. [MR #23](https://gitlab.com/aweframework/awe-react/-/merge_requests/23) (Pablo Javier García Mora)
- Pick version 4.4.7 of AWE. [MR #22](https://gitlab.com/aweframework/awe-react/-/merge_requests/22) (Pablo Javier García Mora)
- Difference between editing rows and selected rows. [MR #21](https://gitlab.com/aweframework/awe-react/-/merge_requests/21) (Pablo Javier García Mora)
- Add Grid actions. [MR #20](https://gitlab.com/aweframework/awe-react/-/merge_requests/20) (Pablo Javier García Mora)
- Add Multioperation Grid. [MR #19](https://gitlab.com/aweframework/awe-react/-/merge_requests/19) (Pablo Javier García Mora)
- The app doesn't work with context-path. [MR #18](https://gitlab.com/aweframework/awe-react/-/merge_requests/18) (Pablo Javier García Mora)
- Validation with format `maxlength: {value: 4, type: 'integer'}` is not working. [MR #17](https://gitlab.com/aweframework/awe-react/-/merge_requests/17) (Pablo Javier García Mora)
- Fix checkbox and radio button styles (changed after primeflex update). [MR #16](https://gitlab.com/aweframework/awe-react/-/merge_requests/16) (Pablo Javier García Mora)
- Add grid footer. [MR #15](https://gitlab.com/aweframework/awe-react/-/merge_requests/15) (Pablo Javier García Mora)
- Add Grid line number. [MR #14](https://gitlab.com/aweframework/awe-react/-/merge_requests/14) (Pablo Javier García Mora)
- Update primereact version to latest one. [MR #13](https://gitlab.com/aweframework/awe-react/-/merge_requests/13) (Pablo Javier García Mora)
- Add Help Viewer widget. [MR #12](https://gitlab.com/aweframework/awe-react/-/merge_requests/12) (Pablo Javier García Mora)
- Add Log Viewer widget. [MR #11](https://gitlab.com/aweframework/awe-react/-/merge_requests/11) (Pablo Javier García Mora)
- Define initial value for suggest and file uploader. [MR #10](https://gitlab.com/aweframework/awe-react/-/merge_requests/10) (Pablo Javier García Mora)
- Add Pivot Table component. [MR #9](https://gitlab.com/aweframework/awe-react/-/merge_requests/9) (Pablo Javier García Mora)
- Add File Manager component. [MR #8](https://gitlab.com/aweframework/awe-react/-/merge_requests/8) (Pablo Javier García Mora)
- Add Accordion component. [MR #7](https://gitlab.com/aweframework/awe-react/-/merge_requests/7) (Pablo Javier García Mora)
- Add resizable component. [MR #6](https://gitlab.com/aweframework/awe-react/-/merge_requests/6) (Pablo Javier García Mora)
- Add suggest `strict=false` functionality. [MR #5](https://gitlab.com/aweframework/awe-react/-/merge_requests/5) (Pablo Javier García Mora)
- Fix grid reloading data when changing tabs. [MR #4](https://gitlab.com/aweframework/awe-react/-/merge_requests/4) (Pablo Javier García Mora)
- Update primereact version to latest one. [MR #3](https://gitlab.com/aweframework/awe-react/-/merge_requests/3) (Pablo Javier García Mora)
- When session is lost, screens remains empty and there's no interactivity with user. [MR #2](https://gitlab.com/aweframework/awe-react/-/merge_requests/2) (Pablo Javier García Mora)
- Add Grid bottom buttons. [MR #1](https://gitlab.com/aweframework/awe-react/-/merge_requests/1) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.18
*25/10/2024*

- Highcharts doesn't change locales when changing language. [MR #59](https://gitlab.com/aweframework/awe-react/-/merge_requests/59) (Pablo Javier García Mora)
- Show 4xx and 5xx screens when page generation returns an error. [MR #58](https://gitlab.com/aweframework/awe-react/-/merge_requests/58) (Pablo Javier García Mora)
- AweButtonRadio doesn't change labels when changing the language. [MR #57](https://gitlab.com/aweframework/awe-react/-/merge_requests/57) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.17
*22/10/2024*

- Fix tree grid issue with Columns module import. [MR #56](https://gitlab.com/aweframework/awe-react/-/merge_requests/56) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.16
*21/10/2024*

- Change message position to bottom. [MR #55](https://gitlab.com/aweframework/awe-react/-/merge_requests/55) (Pablo Javier García Mora)
- The tree is not working when the parent id is null instead of blank. [MR #54](https://gitlab.com/aweframework/awe-react/-/merge_requests/54) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.15
*17/10/2024*

- Wizard doesn't show the step where you are. [MR #53](https://gitlab.com/aweframework/awe-react/-/merge_requests/53) (Pablo Javier García Mora)
- Input Numeric shows always a 0 if no value defined. [MR #52](https://gitlab.com/aweframework/awe-react/-/merge_requests/52) (Pablo Javier García Mora)
- Text view doesn't show the value if there is no label defined. [MR #51](https://gitlab.com/aweframework/awe-react/-/merge_requests/51) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.14
*16/10/2024*

- Bump AWE to 4.9.1 version. [MR #50](https://gitlab.com/aweframework/awe-react/-/merge_requests/50) (Pablo Javier García Mora)
- Manage event to be launched once each time they are called, instead of keeping event in model. [MR #49](https://gitlab.com/aweframework/awe-react/-/merge_requests/49) (Pablo Javier García Mora)
- Uploader doesn't take into account context-path when trying to upload files. [MR #48](https://gitlab.com/aweframework/awe-react/-/merge_requests/48) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.13
*30/09/2024*

- Bump awe version to 4.9.0. [MR #47](https://gitlab.com/aweframework/awe-react/-/merge_requests/47) (Pablo Javier García Mora)
- Force selection on suggest (do not allow a not selected value). [MR #46](https://gitlab.com/aweframework/awe-react/-/merge_requests/46) (Pablo Javier García Mora)
- Add-class, Remove-class and Toggle-class actions are being launched a lot of times. [MR #45](https://gitlab.com/aweframework/awe-react/-/merge_requests/45) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.12
*20/09/2024*

- The second time a message is shown and hides the action gets blocked. [MR #44](https://gitlab.com/aweframework/awe-react/-/merge_requests/44) (Pablo Javier García Mora)
- Generate a new module with react client generic screens. [MR #43](https://gitlab.com/aweframework/awe-react/-/merge_requests/43) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.11
*06/09/2024*

- Generate Avatar component. [MR #42](https://gitlab.com/aweframework/awe-react/-/merge_requests/42) (Pablo Javier García Mora)
- Generate help tooltip on buttons and help icons. [MR #40](https://gitlab.com/aweframework/awe-react/-/merge_requests/40) (Pablo Javier García Mora)
- Error generating help screens. [MR #39](https://gitlab.com/aweframework/awe-react/-/merge_requests/39) (Pablo Javier García Mora)
- Add a new main stunning style. [MR #38](https://gitlab.com/aweframework/awe-react/-/merge_requests/38) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.10
*04/06/2024*

- Fix release notes text generation. [MR #33](https://gitlab.com/aweframework/awe-react/-/merge_requests/33) (Pablo Javier García Mora)

# Changelog for AWE React 0.0.9
*04/06/2024*

- Pick new AWE version. [MR #32](https://gitlab.com/aweframework/awe-react/-/merge_requests/32) (Pablo Javier García Mora)
- Add release note generator. [MR #31](https://gitlab.com/aweframework/awe-react/-/merge_requests/31) (Pablo Javier García Mora)

