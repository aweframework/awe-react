import FormService from "../../../main/js/services/FormService";

describe('awe-client-react/src/test/js/services/FormServiceTest.jsx', () => {
  const service = new FormService();

  beforeEach(function () {
  });

  it('should get all form actions', () => {
    let actions = service.getActions();
    expect(Object.keys(actions).length).toBe(23);
  });

});
