import React from 'react';
import {DEFAULT_SETTINGS} from "../../../main/js/redux/actions/settings";
import {AweView} from "../../../main/js/components/AweView";
import {renderWithProviders} from "../test-utils";

describe('awe-client-react/src/test/js/components/AweViewTest.jsx', () => {

  const preloadedState = {
    settings: DEFAULT_SETTINGS
  };

  it('renders Awe View component', () => {
    renderWithProviders(<AweView/>, {preloadedState});

    // fails
    expect(document.querySelector(".p-progress-spinner")).not.toBeNull();
  });
});
