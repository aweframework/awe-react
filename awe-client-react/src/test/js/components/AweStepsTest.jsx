import React from 'react';
import {DEFAULT_SETTINGS} from "../../../main/js/redux/actions/settings";
import {renderWithProviders} from "../test-utils";
import AweSteps from "../../../main/js/components/AweSteps";

describe('awe-client-react/src/test/js/components/AweStepsTest.jsx', () => {

  const preloadedState = {
    settings: DEFAULT_SETTINGS,
    components: {
      steps: {
        address: {component: 'steps', view: 'report'},
        model: {values: [{label: 'test', value: 'test', selected: true}]},
        elementList: [],
        attributes: {
        },
        validationRules: {
          required: false
        },
        specificAttributes: {sort: []}
      }
    }
  };

  it('renders Awe Steps component', () => {
    renderWithProviders(<AweSteps id="steps"/>, {preloadedState});

    // fails
    expect(document.querySelector("nav.p-steps")).not.toBeNull();
  });
});
