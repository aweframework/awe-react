import React from 'react';
import {cleanup, fireEvent, screen} from '@testing-library/react';
import {DEFAULT_SETTINGS} from "../../../main/js/redux/actions/settings";
import {renderWithProviders} from "../test-utils";
import AweLogViewer from "../../../main/js/widgets/AweLogViewer";

describe('awe-client-react/src/test/js/widgets/AweLogViewerTest.jsx', () => {

  afterAll(cleanup);

  const preloadedState = {
    settings: DEFAULT_SETTINGS,
    components: {
      logViewer: {
        address: {component: 'logViewer', view: 'report'},
        model: {values: []},
        attributes: {
          autorefresh: 1
        },
        specificAttributes: {sort: []}
      }
    }
  };

  beforeEach(() => {
    // Install clock
    jasmine.clock().install();

    spyOn(window, "fetch").and.returnValue(Promise.resolve({
      headers: {
        get: () => 'application/json;charset=UTF-8'
      },
      status: 200,
      ok: true,
      json: () => Promise.resolve([{type: "log-delta", parameters: {log:["tutu", "lala"]}}])
    }));
  });

  afterEach(() => {
    // Uninstall clock
    jasmine.clock().uninstall();
  })

  it('renders AWE Log Viewer widget', () => {
    renderWithProviders(<AweLogViewer id="logViewer"/>, {preloadedState});

    // Install clock
    jasmine.clock().tick(150);

    expect(screen.findByPlaceholderText("Search")).toBeDefined();
  });

  it('renders AWE Log Viewer widget and disables log autorefresh', () => {
    renderWithProviders(<AweLogViewer id="logViewer"/>, {preloadedState});

    // Install clock
    jasmine.clock().tick(150);

    expect(screen.findByPlaceholderText("Search")).toBeDefined();

    fireEvent.click(screen.getByTestId("autoload-button"));
  });
});
