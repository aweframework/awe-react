import React from 'react';
import {renderWithProviders} from "../test-utils";
import AweFileManager from "../../../main/js/widgets/AweFileManager";

describe('awe-client-react/src/test/js/widgets/AweFileManagerTest.jsx', () => {

  it('renders AWE File Manager widget', () => {
    renderWithProviders(<AweFileManager id="file-manager"/>, {});

    expect(document.querySelector("#file-manager")).not.toBeNull();
    expect(document.querySelector("iframe.expand")).not.toBeNull();
  });

  it('renders AWE File Manager widget without id', () => {
    renderWithProviders(<AweFileManager/>, {});

    expect(document.querySelector("iframe.expand")).not.toBeNull();
  });
});
