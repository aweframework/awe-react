import {render} from '@testing-library/react';

import {Columns} from '../../../main/js/utilities/structure';

describe('awe-client-react/src/test/js/columns/ColumnTextViewTest.jsx', () => {

  it('renders Column Text View component', () => {
    render(Columns({
      component: 'text-view',
      model: {values: []},
      numberFormat: {},
      updateModelWithDependencies: jasmine.createSpy("updateModel"),
      updateAttributes: jasmine.createSpy("updateAttributes"),
      addActionsTop: jasmine.createSpy("addActionsTop"),
      address: {component: 'text-view', view: 'report', column: 'column', row: 'row'},
      t: jasmine.createSpy("t"),
      settings: {}
    }, {value: "test"}, true));

    // fails
    expect(document.querySelector("button")).not.toBeNull();
  });
});
