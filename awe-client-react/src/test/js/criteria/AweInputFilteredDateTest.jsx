import {DEFAULT_SETTINGS} from "../../../main/js/redux/actions/settings";
import React from "react";
import {renderWithProviders} from "../test-utils";
import AweInputFilteredDate from "../../../main/js/criteria/AweInputFilteredDate";
import {cleanup, screen} from "@testing-library/react";

describe('awe-client-react/src/test/js/criteria/AweInputFilteredDateTest.jsx', () => {

  afterAll(cleanup);

  const preloadedState = {
    settings: DEFAULT_SETTINGS,
    components: {
      filteredDate: {
        address: {component: 'filteredDate', view: 'report'},
        model: {values: [{label: 'test', value: 'test', selected: true}]},
        attributes: {
          placeholder: "placeholder",
          readonly: false,
          helpImage: "test"
        },
        validationRules: {
          required: false
        },
        specificAttributes: {sort: []}
      }
    }
  };

  it('renders Awe Input FilteredDate component', () => {
    renderWithProviders(<AweInputFilteredDate id="filteredDate"/>, {preloadedState});

    // check
    expect(screen.findByRole("combobox")).not.toBeNull();
  });

});
