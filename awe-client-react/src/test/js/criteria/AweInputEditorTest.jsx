import {DEFAULT_SETTINGS} from "../../../main/js/redux/actions/settings";
import React from "react";
import {renderWithProviders} from "../test-utils";
import AweInputEditor from "../../../main/js/criteria/AweInputEditor";

describe('awe-client-react/src/test/js/criteria/AweInputEditorTest.jsx', () => {

  const preloadedState = {
    settings: DEFAULT_SETTINGS,
    components: {
      editor: {
        address: {component: 'editor', view: 'report'},
        model: {values: [{label: 'test', value: 'test', selected: true}]},
        attributes: {
          placeholder: "placeholder",
          readonly: false,
          helpImage: "test"
        },
        validationRules: {
          required: false
        },
        specificAttributes: {sort: []}
      }
    }
  };

  it('renders Awe Input Editor component', () => {
    renderWithProviders(<AweInputEditor id="editor"/>, {preloadedState});

    // check
    expect(document.querySelector("#editor")).not.toBeNull();
  });

});
