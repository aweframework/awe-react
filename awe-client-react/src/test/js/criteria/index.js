import './AweInputUploaderTest';
import './AweSuggestTest';
import './AweSuggestMultipleTest';
import './AweInputTextTest';
import './AweInputNumericTest';
import './AweInputTextViewTest';
import './AweInputCheckboxTest';
import './AweInputRadioTest';
import './AweButtonCheckboxTest';
import './AweButtonRadioTest';
import './AweSelectTest';
import './AweSelectMultipleTest';
import './AweInputColorTest';
import './AweInputDateTest';
import './AweInputFilteredDateTest';
import './AweInputTimeTest';
import './AweInputPasswordTest';
import './AweInputTextareaTest';
import './AweInputEditorTest';
