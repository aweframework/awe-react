import en from './highcharts-en.json';
import es from './highcharts-es.json';
import fr from './highcharts-fr.json';

global.HighchartsLocale = {en, es, fr};
