const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const dir = path.join(__dirname, "webpack");

module.exports = {
  entry: {
    "bundle": path.join(dir, "awe.config.js"),
    "locals-es": path.join(dir, "locals-es.config.js"),
    "locals-en": path.join(dir, "locals-en.config.js"),
    "locals-eu": path.join(dir, "locals-eu.config.js"),
    "locals-fr": path.join(dir, "locals-fr.config.js")
  },
  output: {
    filename: "js/[name].js",
    path: path.join(__dirname, 'target', 'classes', 'static'),
    publicPath: "../"
  },
  module: {
    rules: [
      {test: /\.(tsx|ts|jsx|js)$/, exclude: /node_modules/, use: 'babel-loader'},
      {test: /\.(le|c)ss$/, use: [MiniCssExtractPlugin.loader, "css-loader", "postcss-loader", "less-loader"]},
      {test: /\.(jpg|gif|png)$/, type: 'asset/resource', generator: { filename: 'images/[hash][ext][query]'}},
      {test: /\.woff[2]*?(\?v=[0-9]\.[0-9]\.[0-9])?$/, type: 'asset/resource', generator: {filename: "fonts/[hash][ext][query]"}},
      {test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, type: 'asset/resource', generator: {filename: "fonts/[hash][ext][query]"}},
    ]
  },
  resolve: {
    extensions: [".jsx", ".js", ".css", ".less", "*"],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "css/[name].css"
    }),
    new LodashModuleReplacementPlugin
  ]
};
